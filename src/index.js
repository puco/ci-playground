const { sum } = require("./sum");
const fingerprint = require("./fingerprint.json");
const package = require("../package.json");

console.log("Hi this is test");
console.log(
  `Running: v${package.version}, fingerprint: ${fingerprint.reference}`
);
console.log(`The sum is: ${sum(process.argv.slice(2))}`);
