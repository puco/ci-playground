function sum(args) {
  return (args || []).reduce((acc, i) => acc + parseInt(i), 0);
}
function product(args) {
  return (args || []).reduce((acc, i) => acc * parseInt(i), 1);
}

module.exports = { sum, product };
