# only install packages
FROM node:alpine as cache
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

# runner cache
FROM node:alpine as runner-cache
WORKDIR /app
COPY package.json package-lock.json ./
RUN npm ci

# build and fingerprint
FROM cache as builder
COPY . .
ARG FINGERPRINT=manual
RUN echo "{ \"reference\": \"${FINGERPRINT}\" }" > src/fingerprint.json

# tester
FROM builder as tester
RUN npx jest

# run
FROM runner-cache as runner
COPY --from=builder /app/src src/
ENTRYPOINT [ "node", "src" ]
