# CI Playground

[![coverage report](https://gitlab.com/puco/ci-playground/badges/dev/coverage.svg?style=flat-square)](https://gitlab.com/puco/ci-playground/commits/dev)


The purpose of this respository is to test CI / CD

Requirements:
- all in docker
- multi-stage build (small contained container, separate tests)
- proper separation of `dev` and `prod` registries
- caching
- release process
- semver tagging of production containers

